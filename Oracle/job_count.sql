DECLARE
   jobs_count INTEGER;
BEGIN
   SELECT COUNT(*) INTO jobs_count FROM jobs;

     DBMS_OUTPUT.PUT_LINE (
      'There are ' || 
      jobs_count ||
      ' jobs.');
END;