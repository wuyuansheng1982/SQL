SELECT * FROM
  (SELECT count(*), p.spid "UNIX PID", s.username, s.module, s.sid, s.serial#, q.SQL_TEXT "Current SQL"
  FROM
    v$active_session_history h,
    v$session s,
    v$sql q,
    v$process p 
  WHERE
    h.session_id = s.sid
  AND h.session_serial# = s.serial#
  AND session_state= 'ON CPU'
  AND p.addr = s.paddr
  AND q.sql_id=s.PREV_SQL_ID
  AND sample_time > sysdate - interval '15' minute
  GROUP BY p.spid,s.username, s.module, s.sid, s.serial#,q.SQL_TEXT
  ORDER BY count(*) desc)
where rownum <= 5;

