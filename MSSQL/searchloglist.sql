CREATE PROCEDURE [dbo].[SearchLogList]
	@IP nvarchar(20),
    @ZKH nvarchar(50),
	@PageSize int, -- 每页数据行数
	@toPage int --要转到的页码第一页为       
AS

declare @strSql nvarchar(100)
declare @strWhere nvarchar(100)

set @strSql = '[IpLog]'

set @strWhere = '1=1'

if @IP <> '' 
begin
	set @strWhere = @strWhere + ' and IP=''' + LTRIM(RTRIM(@IP)) + ''''
end	

if @ZKH <> '' 
begin
	set @strWhere = @strWhere + ' and ZKH=''' + LTRIM(RTRIM(@ZKH))+ ''''
end	

exec PageCut @strSql,'ID','','*',@strWhere,'ID asc',@PageSize,@toPage